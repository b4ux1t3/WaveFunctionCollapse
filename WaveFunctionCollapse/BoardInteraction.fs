﻿module WaveFunctionCollapse.BoardInteraction

open System
open WaveFunctionCollapse.TileDefinitions
open WaveFunctionCollapse.Types

let indexToCoord width index =
    (index / width, index % width) |> Coordinate

let coordToIndex width coord =
    match coord with
    | Coordinate (row, col) -> row * width + col

let getTileChoicesForCoord (board: Board) coord =
    let coordToIndexThisBoard = coordToIndex board.width
    let row, col =
        match coord with
        | Coordinate (row, col) -> row, col
    let northTile =
        match row with
        | n when n > 0 -> board.tiles[(coordToIndexThisBoard coord)-board.width]
        | _ -> board.tiles[(board.height-1) * board.width + col]
    let eastTile =
        match col with
        | n when n < board.width-1 -> board.tiles[(coordToIndexThisBoard coord) + 1]
        | _ -> board.tiles[row * board.width]
    let southTile =
        match row with
        | n when n < board.height-1 -> board.tiles[(coordToIndexThisBoard coord)+board.width]
        | _ -> board.tiles[row * board.width + col]
    let westTile =
        match col with
        | n when n > 0 -> board.tiles[(coordToIndexThisBoard coord) - 1]
        | _ -> board.tiles[row * board.width + board.width - 1]
        
    let northChoices =
            match northTile with
            | Resolved tile -> extractChoices tile.neighbors.south
            | Unresolved tileChoices -> extractChoices tileChoices
    let eastChoices =
            match eastTile with
            | Resolved tile -> extractChoices tile.neighbors.west
            | Unresolved tileChoices -> extractChoices tileChoices
    let southChoices =
            match southTile with
            | Resolved tile -> extractChoices tile.neighbors.north
            | Unresolved tileChoices -> extractChoices tileChoices
    let westChoices =
            match westTile with
            | Resolved tile -> extractChoices tile.neighbors.east
            | Unresolved tileChoices -> extractChoices tileChoices
    [|
        yield! northChoices
        yield! eastChoices
        yield! southChoices    
        yield! westChoices
    |]
    |> set
    |> Set.toArray

let getTileChoicesForIndex (board: Board) index =
    match index < board.tiles.Length with
    | true -> 
        index
        |> indexToCoord board.width
        |> getTileChoicesForCoord board
        |> Some
    | false -> None
    
let tryCollapseTile (dict: Map<string, TileDefinition>) (board: Board) index =
    let choices =
        match getTileChoicesForIndex board index with
        | Some c -> c
        | None -> Array.empty
    let selectedIndex = Random.Shared.Next(choices.Length)
    let selection =
        match choices[selectedIndex] with
        | TileIdentifier id ->
            match dict.TryGetValue id with
            | true, def -> Some def
            | false, _ -> None
                
    match selection with
    | Some def->
        let newTiles = board.tiles |> Array.mapi (
            fun i t ->
                match i = selectedIndex with
                | true -> def |> defToTile |> Resolved
                | false -> board.tiles[i]
            )
        {board with tiles = newTiles} |> Some
    | _ -> None