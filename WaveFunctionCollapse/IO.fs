﻿module WaveFunctionCollapse.IO
open FSharp.Json
open WaveFunctionCollapse.TileDefinitions
open WaveFunctionCollapse.Types

type RawTileDefinition = {
    identifier: string
    validNeighborsUp: string array
    validNeighborsDown: string array
    validNeighborsLeft: string array
    validNeighborsRight: string array
}
 
let parseRawDefinition s (rawDef: RawTileDefinition) : TileDefinition =
    {
        identifier = s |> makeIdentifier;
        validNeighborsNorth = rawDef.validNeighborsDown |> Array.map makeIdentifier;
        validNeighborsSouth = rawDef.validNeighborsDown |> Array.map makeIdentifier;
        validNeighborsWest = rawDef.validNeighborsDown |> Array.map makeIdentifier;
        validNeighborsEast = rawDef.validNeighborsDown |> Array.map makeIdentifier;
    }


let loadRawDefinitions str =
    Json.deserialize<Map<string, RawTileDefinition>> str
    
let parseRawDictionary rawDict =
    rawDict |> Map.map parseRawDefinition
    
let makeTileDefinitions str =
    str |> loadRawDefinitions |> parseRawDictionary