module WaveFunctionCollapse.TileDefinitions 
open WaveFunctionCollapse.Types
type TileDefinition = {
    identifier: TileIdentifier
    validNeighborsNorth: TileIdentifier array
    validNeighborsEast: TileIdentifier array
    validNeighborsSouth: TileIdentifier array
    validNeighborsWest: TileIdentifier array
}
let defToNeighbors tileDef: ValidNeighbors =
    {
        north =
            tileDef.validNeighborsNorth
            |> Array.map (fun tId -> match tId with TileIdentifier s -> s |> TileIdentifier)
            |> TileChoices
        east = tileDef.validNeighborsEast
            |> Array.map (fun tId -> match tId with TileIdentifier s -> s |> TileIdentifier)
            |> TileChoices
        south = tileDef.validNeighborsSouth
            |> Array.map (fun tId -> match tId with TileIdentifier s -> s |> TileIdentifier)
            |> TileChoices
        west = tileDef.validNeighborsWest
            |> Array.map (fun tId -> match tId with TileIdentifier s -> s |> TileIdentifier)
            |> TileChoices
    }
let defToTile tileDef: Tile =
    {
     identifier = tileDef.identifier
     neighbors = defToNeighbors tileDef 
    }

let getTileOptionsFromDictionary (dict: Map<string, TileDefinition>) tileId =
    match dict.TryGetValue tileId with
    | true, def ->
        [|
            yield! def.validNeighborsNorth
            yield! def.validNeighborsEast
            yield! def.validNeighborsSouth
            yield! def.validNeighborsWest
        |]
        |> TileChoices
        |> Some
    | false, _ -> None

let getTileOptionsFromDirection (tileDef: TileDefinition) direction =
    match direction with
    | North -> tileDef.validNeighborsNorth
    | East -> tileDef.validNeighborsEast
    | South -> tileDef.validNeighborsSouth
    | West -> tileDef.validNeighborsWest
    
let getTileOptionsFromDictWithDirection (dict: Map<string, TileDefinition>) tileId direction =
    match dict.TryGetValue tileId with
    | true, def ->
        getTileOptionsFromDirection def direction
        |> TileChoices
        |> Some
    | false, _ -> None

let extractValue kvp = match kvp with _,v -> v

let extractId (def: TileDefinition) = def.identifier

let getAllTileOptions (dict: Map<string, TileDefinition>) =
    dict
    |> Map.toArray
    |> Array.map extractValue
    |> Array.map extractId
    |> TileChoices


let getAllAdjacentTileOptions (dict: Map<string, TileDefinition>) north south east west =
    let northOptions = 
        match getTileOptionsFromDictWithDirection dict north North with 
        | Some arr -> arr
        | None -> Array.empty |> TileChoices
    let eastOptions = 
        match getTileOptionsFromDictWithDirection dict east East with
        | Some arr -> arr
        | None -> Array.empty |> TileChoices
    let southOptions = 
        match getTileOptionsFromDictWithDirection dict south South with
        | Some arr -> arr
        | None -> Array.empty |> TileChoices 
    let westOptions = 
        match getTileOptionsFromDictWithDirection dict west West with
        | Some arr -> arr
        | None -> Array.empty |> TileChoices
    
    let allOptions =
        [|
            yield! match northOptions with TileChoices opts -> opts
            yield! match eastOptions with TileChoices opts -> opts
            yield! match southOptions with TileChoices opts -> opts
            yield! match westOptions with TileChoices opts -> opts
        |] |> set
        
    allOptions |> Set.toArray |> TileChoices
    
    
    
    
        
        