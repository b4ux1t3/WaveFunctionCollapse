module WaveFunctionCollapse.Types 
type TileIdentifier = TileIdentifier of string
type TileChoices = TileChoices of TileIdentifier array
type ValidNeighbors = {
    north: TileChoices
    east: TileChoices
    south: TileChoices
    west: TileChoices
}
type Tile = {
    identifier: TileIdentifier
    neighbors: ValidNeighbors
}
    
type TileState =
    | Resolved of Tile
    | Unresolved of TileChoices
type Board = { 
    width: int 
    height: int
    tiles: TileState array
}
type Coordinate = Coordinate of int * int
type Direction =
    | North
    | East
    | South
    | West

let makeIdentifier s = s |> TileIdentifier
let makeTile id neighbors =
    {
        identifier = id
        neighbors = neighbors 
    }
    
let extractChoices tileChoices =
    match tileChoices with TileChoices choices -> choices
       