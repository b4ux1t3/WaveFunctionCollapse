﻿// For more information see https://aka.ms/fsharp-console-apps
open WaveFunctionCollapse.IO
open WaveFunctionCollapse.TileDefinitions
open WaveFunctionCollapse.Types
open WaveFunctionCollapse.BoardInteraction

let testInput = """
{
    "water_tile": {
        "identifier": "water_tile",
        "validNeighborsUp": [
            "water_tile"
        ],
        "validNeighborsDown": [
            "water_tile"
        ],
        "validNeighborsLeft": [
            "water_tile"
        ],
        "validNeighborsRight": [
            "water_tile"
        ]
    },
    "ground_tile": {
        "validNeighborsUp": [
            "ground_tile"
        ],
        "validNeighborsDown": [
            "ground_tile"
        ],
        "validNeighborsLeft": [
            "ground_tile"
        ],
        "validNeighborsRight": [
            "ground_tile"
        ]
    },
    "water_west_ground_east": {
        "validNeighborsUp": [
            "water_west_ground_east"
        ],
        "validNeighborsDown": [
            "water_west_ground_east"
        ],
        "validNeighborsLeft": [
            "water_tile"
        ],
        "validNeighborsRight": [
            "ground_tile"
        ]
    }
}

"""
let testDict = makeTileDefinitions testInput

let getTileOptions = getTileOptionsFromDictionary testDict

let allOptions = getAllTileOptions testDict

let columns = 3
let rows = 3

let initialTiles =
    Array.create (rows * columns) allOptions
    
let initialBoard : Board = { width = 3; height = 3; tiles = initialTiles |> Array.map Unresolved }
let getChoicesBoardIndex = getTileChoicesForIndex initialBoard
let choices = getChoicesBoardIndex 4

printfn $"%A{testDict}"